/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import com.mycompany.oxnetbean.Player;
import com.mycompany.oxnetbean.Table;

/**
 *
 * @author Windows10
 */
public class TableUnitTest {
    
    public TableUnitTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }
    
   
  public  void testRow1Win(){
        Player o = new Player('o');
        Player x = new Player('x');
        Table tb = new Table(o,x);
        tb.setRowCol(1, 1);
        tb.setRowCol(1, 2);
        tb.setRowCol(1, 3);
        assertEquals(true,tb.checkWin());
    }
  public  void testRow2Win(){
        Player o = new Player('o');
        Player x = new Player('x');
        Table tb = new Table(o,x);
        tb.setRowCol(1, 1);
        tb.setRowCol(1, 2);
        tb.setRowCol(1, 3);
        tb.setRowCol(2, 1);
        tb.setRowCol(2, 2);
        tb.setRowCol(2, 3);
        assertEquals(true,tb.checkWin());
    }
   public  void testRow3Win(){
        Player o = new Player('o');
        Player x = new Player('x');
        Table tb = new Table(o,x);
        tb.setRowCol(2, 1);
        tb.setRowCol(2, 2);
        tb.setRowCol(2, 3);
        tb.setRowCol(3, 1);
        tb.setRowCol(3, 2);
        tb.setRowCol(3, 3);
        assertEquals(true,tb.checkWin());
    }
     public void testCol1Win() {
        Player o = new Player('o');
        Player x = new Player('x');
        Table tb = new Table(o,x);
        tb.setRowCol(1, 1);
        tb.setRowCol(2, 1);
        tb.setRowCol(3, 1);
        assertEquals(true,tb.checkWin());
    }
      public void testCol2Win() {
        Player o = new Player('o');
        Player x = new Player('x');
        Table tb = new Table(o,x);
        tb.setRowCol(1, 2);
        tb.setRowCol(2, 2);
        tb.setRowCol(3, 2);
        assertEquals(true,tb.checkWin());
    }
      public void testCol3Win() {
        Player o = new Player('o');
        Player x = new Player('x');
        Table tb = new Table(o,x);
        tb.setRowCol(1, 3);
        tb.setRowCol(2, 3);
        tb.setRowCol(3, 3);
        assertEquals(true,tb.checkWin());
    }
      public void testX1Win() {
        Player o = new Player('o');
        Player x = new Player('x');
        Table tb = new Table(o,x);
        tb.setRowCol(1, 3);
        tb.setRowCol(2, 3);
        tb.setRowCol(1, 1);
        tb.setRowCol(2, 2);
        tb.setRowCol(3, 3);
        assertEquals(true,tb.checkWin());
    }
      public void testX2Win() {
        Player o = new Player('o');
        Player x = new Player('x');
        Table tb = new Table(o,x);
        tb.setRowCol(1, 3);
        tb.setRowCol(2, 2);
        tb.setRowCol(3, 1);
        assertEquals(true,tb.checkWin());
    }
       public void testSwitchPlayer(){
        Player o = new Player('o');
        Player x = new Player('x');
        Table tb = new Table(o,x);
        tb.switchPlayer();
        assertEquals('x',tb.getCurrentPlayer().getName());
        
    }



   

 

  
}
